﻿namespace DemoGit
{
    public static class ClassPidaras
    {
        public static int Add(int value1, int value2)
        {
            return value1 + value2;
        }

        public static int Abs(int input)
        {
            if (input < 0)
            {
                return input * -1;
            }
            else
            {
                return input;
            }
        }
    }
}
